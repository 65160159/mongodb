/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.java.myprojectapp;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.util.Set;

/**
 *
 * @author informatics
 */
public class MongoDisplayAllCollections {

        public static void main(String[] args) {
            try {
                MongoClient mongo = new MongoClient("localhost", 27017);
                DB db = mongo.getDB("test");
                Set<String> tables = db.getCollectionNames();
                for (String coll : tables) {
                    System.out.println(coll);
                }
            } catch (Exception e) {
            }
        }
    }