/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.java.myprojectapp;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import javax.swing.JOptionPane;
import java.util.*;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author informatics
 */
public class ShowDocumentCondition extends javax.swing.JFrame {

    /**
     * Creates new form MyFormApp
     */
    public ShowDocumentCondition() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ConnectDB = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        List = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListFixDoc = new javax.swing.JList<>();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        TextPrice = new javax.swing.JTextField();
        ListDoc = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ConnectDB");

        ConnectDB.setToolTipText("ConnectDB");
        ConnectDB.setName("ConnectDB"); // NOI18N

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel1.setText("ค้นหาแบบระบุเงื่อนไขแสดงข้อความระบุรหัส Employee (emp_id):");

        jButton1.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        jButton1.setText("ค้นหา");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        List.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ListMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(List);

        jScrollPane3.setViewportView(ListFixDoc);

        jButton2.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        jButton2.setText("Exit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel2.setText("แสดงข้อมูล Document ทั้งหมด");

        TextPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextPriceActionPerformed(evt);
            }
        });

        ListDoc.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        ListDoc.setText("แสดง Document");
        ListDoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ListDocActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ConnectDBLayout = new javax.swing.GroupLayout(ConnectDB);
        ConnectDB.setLayout(ConnectDBLayout);
        ConnectDBLayout.setHorizontalGroup(
            ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConnectDBLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 461, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(ConnectDBLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TextPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(32, 32, 32))
            .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(ConnectDBLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(321, Short.MAX_VALUE)))
            .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(ConnectDBLayout.createSequentialGroup()
                    .addGap(43, 43, 43)
                    .addComponent(ListDoc)
                    .addContainerGap(679, Short.MAX_VALUE)))
        );
        ConnectDBLayout.setVerticalGroup(
            ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConnectDBLayout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2)
                    .addGroup(ConnectDBLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(TextPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(57, Short.MAX_VALUE))
            .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(ConnectDBLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(jLabel2)
                    .addContainerGap(304, Short.MAX_VALUE)))
            .addGroup(ConnectDBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(ConnectDBLayout.createSequentialGroup()
                    .addGap(76, 76, 76)
                    .addComponent(ListDoc)
                    .addContainerGap(251, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ConnectDB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(ConnectDB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        ConnectDB.getAccessibleContext().setAccessibleName("ConnectDB");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        setVisible(false);//TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("BigC");
        if (TextPrice.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "กรุณาพิมพ์");
        } else {
            String tmp = TextPrice.getText();
            DBCollection table = db.getCollection("Employee");
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("emp_id", tmp);
            DBCursor cursor = table.find(searchQuery);
            DefaultListModel modeil1 = new DefaultListModel();
            
            while (cursor.hasNext()) {
                modeil1.addElement(cursor.next());
            }
            ListFixDoc.setModel(modeil1);
        }
    } catch (Exception e) {
        // e.printStackTrace();
    }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void TextPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TextPriceActionPerformed

    private void ListDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ListDocActionPerformed
        try {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("BigC");
        DBCollection table = db.getCollection("Employee");
        DBCursor cursor = table.find();
        DefaultListModel modeil1 = new DefaultListModel();
        while (cursor.hasNext()) {
            modeil1.addElement(cursor.next());
        }
        ListDoc.setModel(modeil1);
        // Adding a ListSelectionListener to ListDoc
        ListDoc.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                if (!event.getValueIsAdjusting()) {
                    // Get the selected data from ListDoc
                    Object selectedData = ListDoc.getSelectedValue();
                    if (selectedData != null) {
                        // Convert the selected data to a BasicDBObject
                        BasicDBObject selectedDBObject = (BasicDBObject) selectedData;
                        // Get the value of "emp_id" from the BasicDBObject
                        String empId = selectedDBObject.getString("emp_id");
                        // Update txtPrice with the empId value
                        TextPrice.setText(empId);
                    }
                }
            }
        });
    } catch(Exception e) {
        // e.printStackTrace();
    } 
    }//GEN-LAST:event_ListDocActionPerformed

    private void ListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ListMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_ListMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

}
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class  

.getName()).log(java.util.logging.Level.SEVERE, null, ex);

} catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class  

.getName()).log(java.util.logging.Level.SEVERE, null, ex);

} catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class  

.getName()).log(java.util.logging.Level.SEVERE, null, ex);

} catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class  

.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowDocumentCondition().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ConnectDB;
    private javax.swing.JList<String> List;
    private javax.swing.JButton ListDoc;
    private javax.swing.JList<String> ListFixDoc;
    private javax.swing.JTextField TextPrice;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    // End of variables declaration//GEN-END:variables
}
