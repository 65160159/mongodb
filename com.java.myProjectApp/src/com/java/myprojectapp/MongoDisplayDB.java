/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.java.myprojectapp;

import com.mongodb.MongoClient;
import java.util.List;

/**
 *
 * @author informatics
 */
public class MongoDisplayDB {

        public static void main(String[] args) {
            try {
                MongoClient mongo = new MongoClient("localhost", 27017);
                List<String> dbs = mongo.getDatabaseNames();
                for (String db : dbs) {
                    System.out.println(db);
                }
            } catch (Exception e) {
            }
        }
    }
